import datetime

from django.shortcuts import render
import random
from django.http import HttpResponse


def home(request):
    return render(request, 'generator/home.html')


def password(request):
    the_password = ''

    characters = list('abcdefghjklmnopqrstuvwxyz')

    if request.GET.get('uppercase'):
        characters.extend(list('ABCDEFGHJKLMNOPQRSTUVWXYZ'))
    if request.GET.get('symbols'):
        characters.extend(list('!?#$%&*_+-'))
    if request.GET.get('numbers'):
        characters.extend(list('0123456789'))

    length = int(request.GET.get('length', 12))

    for x in range(length):
        the_password += random.choice(characters)

    return render(request, 'generator/home.html', {'password': the_password})


def about(request):
    return render(request, 'generator/about.html')